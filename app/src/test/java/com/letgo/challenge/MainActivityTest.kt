package com.letgo.challenge

import androidx.lifecycle.Observer
import com.letgo.challenge.model.MovieList
import com.letgo.challenge.network.API
import com.letgo.challenge.util.State
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.mockito.Mock


class MainActivityTest {

    @Mock
    lateinit var api: API

    private lateinit var getMovieResponse: Observable<MovieList>
    private lateinit var observer: Observer<State>

    @Test
    fun is_api_created() {
        observer = mock()
        getMovieResponse = Observable.create {
            it.serialize()
        }
        api = mock {
            whenever(it.getMovieList(1)).thenReturn(getMovieResponse)
        }
    }

    @Test
    fun check_if_result_list_is_not_empty() {

        getMovieResponse = Observable.create {
            it.serialize()
        }

        getMovieResponse.subscribe({
            assertNotNull(it.results)
        }, {
            //No-op
        })
    }
}
