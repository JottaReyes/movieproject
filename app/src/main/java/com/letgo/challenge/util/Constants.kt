package com.letgo.challenge.util


const val BASE_URL: String = "https://api.themoviedb.org/3/tv/"
const val IMAGE_BASE_URL: String = "https://image.tmdb.org/t/p/w185"