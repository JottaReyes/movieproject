package com.letgo.challenge.util

enum class State {
    DONE,
    LOADING,
    ERROR
}