package com.letgo.challenge.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.letgo.challenge.model.Movie
import com.letgo.challenge.network.API
import io.reactivex.disposables.CompositeDisposable

class MoviesDataSourceFactory(
    private val compositeDisposable: CompositeDisposable,
    private val api: API) : DataSource.Factory<Int, Movie>() {

    val movieDataSourceLiveData = MutableLiveData<MovieDataSource>()

    override fun create(): DataSource<Int, Movie> {
        val moviesDataSource = MovieDataSource(api, compositeDisposable)
        movieDataSourceLiveData.postValue(moviesDataSource)
        return moviesDataSource
    }
}