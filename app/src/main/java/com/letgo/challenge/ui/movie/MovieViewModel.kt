package com.letgo.challenge.ui.movie

import androidx.lifecycle.MutableLiveData
import com.letgo.challenge.base.BaseViewModel
import com.letgo.challenge.model.Movie

class MovieViewModel : BaseViewModel() {

    private val backdropPath = MutableLiveData<String>()
    private val firstAirDate = MutableLiveData<String>()
    private val titleName = MutableLiveData<String>()
    private val originCountry = MutableLiveData<List<String>>()
    private val originalLanguage = MutableLiveData<String>()
    private val originalName = MutableLiveData<String>()
    private val movieOverview = MutableLiveData<String>()
    private val popularity = MutableLiveData<Double>()
    private val posterPath = MutableLiveData<String>()
    private val voteAverage = MutableLiveData<String>()
    private val voteCount = MutableLiveData<Int>()

    fun bind(movie: Movie) {
        backdropPath.value = movie.backdrop_path
        firstAirDate.value = movie.first_air_date
        titleName.value = movie.name
        originCountry.value = movie.origin_country
        originalLanguage.value = movie.original_language
        originalName.value = movie.original_name
        movieOverview.value = movie.overview
        popularity.value = movie.popularity
        posterPath.value = movie.poster_path
        voteAverage.value = movie.vote_average.toString()
        voteCount.value = movie.vote_count
    }

    fun getBackdropPath(): MutableLiveData<String> {
        return backdropPath
    }

    fun getFirstAirDate(): MutableLiveData<String> {
        return firstAirDate
    }

    fun getTitleName(): MutableLiveData<String> {
        return titleName
    }

    fun getOriginCountry(): MutableLiveData<List<String>> {
        return originCountry
    }

    fun getOriginalLanguage(): MutableLiveData<String> {
        return originalLanguage
    }

    fun getOriginalName(): MutableLiveData<String> {
        return originalName
    }

    fun getMovieOverview(): MutableLiveData<String> {
        return movieOverview
    }

    fun getPopularity(): MutableLiveData<Double> {
        return popularity
    }

    fun getPosterPath(): MutableLiveData<String> {
        return posterPath
    }

    fun getVoteAverage(): MutableLiveData<String> {
        return voteAverage
    }

    fun getVoteCount(): MutableLiveData<Int> {
        return voteCount
    }
}