package com.letgo.challenge.ui.movie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.letgo.challenge.R
import com.letgo.challenge.databinding.ActivityMainBinding
import com.letgo.challenge.util.State
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MovieListViewModel
    private lateinit var movieListAdapter: MovieListAdapter

    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(MovieListViewModel::class.java)

        binding.viewModel = viewModel

        initAdapter()
        initState()
    }

    private fun initAdapter() {
        movieListAdapter = MovieListAdapter()
        binding.movieList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.movieList.adapter = movieListAdapter

        viewModel.moviesList.observe(this, Observer {
            movieListAdapter.submitList(it)
        })
    }

    private fun initState() {

        viewModel.getState().observe(this, Observer { state ->

            progress_bar.visibility = if(viewModel.isListEmpty() && state == State.LOADING) View.VISIBLE else View.GONE

            if(viewModel.isListEmpty() && state == State.ERROR) {
                showError(R.string.error_retrieving_movies)
            } else {
                hideError()
            }

            if(!viewModel.isListEmpty()) {
                movieListAdapter.setState(state ?: State.DONE)
            }
        })
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }
}
