package com.letgo.challenge.ui.movie

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.letgo.challenge.base.BaseViewModel
import com.letgo.challenge.data.MovieDataSource
import com.letgo.challenge.data.MoviesDataSourceFactory
import com.letgo.challenge.model.Movie
import com.letgo.challenge.network.API
import com.letgo.challenge.util.State
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MovieListViewModel : BaseViewModel() {

    @Inject
    lateinit var api: API

    private val compositeDisposable = CompositeDisposable()
    private var moviesDataSourceFactory: MoviesDataSourceFactory

    var moviesList : LiveData<PagedList<Movie>>
    val loadingViewVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { retry() }

    init {
        moviesDataSourceFactory = MoviesDataSourceFactory(compositeDisposable, api)
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setInitialLoadSizeHint(40)
            .setEnablePlaceholders(false)
            .build()
        moviesList = LivePagedListBuilder<Int, Movie>(moviesDataSourceFactory, config).build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap<MovieDataSource,
            State>(moviesDataSourceFactory.movieDataSourceLiveData, MovieDataSource::state)

    fun retry() {
        moviesDataSourceFactory.movieDataSourceLiveData.value?.retry()
        loadingViewVisibility.value = View.GONE
    }

    fun isListEmpty(): Boolean {
        return  moviesList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}