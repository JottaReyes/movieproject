package com.letgo.challenge.ui.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.letgo.challenge.R
import com.letgo.challenge.databinding.ItemMovieBinding
import com.letgo.challenge.model.Movie
import com.letgo.challenge.util.IMAGE_BASE_URL
import com.letgo.challenge.util.State


class MovieListAdapter: PagedListAdapter<Movie, RecyclerView.ViewHolder>(
    MovieDiffCallback) {

    private var state = State.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemMovieBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_movie, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let { (holder as ViewHolder).bindMovie(it) }
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    companion object {
        val MovieDiffCallback = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }
        }
    }

    class ViewHolder(private val binding: ItemMovieBinding): RecyclerView.ViewHolder(binding.root) {

        private val viewModel = MovieViewModel()
        private val moviePoster: ImageView = binding.mediaImage

        fun bindMovie(movie: Movie) {
            viewModel.bind(movie)
            binding.viewModel = viewModel
            Glide.with(binding.root.context)
                .load(IMAGE_BASE_URL + movie.poster_path)
                .into(moviePoster)
        }
    }

}