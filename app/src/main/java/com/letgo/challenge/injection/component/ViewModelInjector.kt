package com.letgo.challenge.injection.component

import com.letgo.challenge.injection.module.NetworkModule
import com.letgo.challenge.ui.movie.MovieListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject (movieListViewModel: MovieListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun networkModule(networkModule: NetworkModule): Builder
    }
}