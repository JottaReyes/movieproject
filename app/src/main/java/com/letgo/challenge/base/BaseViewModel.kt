package com.letgo.challenge.base

import androidx.lifecycle.ViewModel
import com.letgo.challenge.injection.component.DaggerViewModelInjector
import com.letgo.challenge.injection.component.ViewModelInjector
import com.letgo.challenge.injection.module.NetworkModule
import com.letgo.challenge.ui.movie.MovieListViewModel

abstract class BaseViewModel: ViewModel() {

    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is MovieListViewModel -> injector.inject(this)
        }
    }

}