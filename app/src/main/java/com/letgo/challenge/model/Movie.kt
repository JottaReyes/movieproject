package com.letgo.challenge.model

data class Movie(
    var backdrop_path: String = "",
    var first_air_date: String = "",
    var genre_ids: List<Int> = listOf(),
    var id: Int = 0,
    var name: String = "",
    var origin_country: List<String> = listOf(),
    var original_language: String = "",
    var original_name: String = "",
    var overview: String = "",
    var popularity: Double = 0.0,
    var poster_path: String = "",
    var vote_average: String = "",
    var vote_count: Int = 0
)