package com.letgo.challenge.network


import com.letgo.challenge.BuildConfig
import com.letgo.challenge.model.MovieList
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface API {

    @GET("popular?language=en-US&api_key="+ BuildConfig.api_key)
    fun getMovieList(@Query("page") page: Int) : Observable<MovieList>
}